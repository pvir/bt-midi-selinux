midigatt
========

Relax Fedora SELinux policies to allow communication between BlueZ and Pipewire
for MIDI.

Install:

    sudo dnf install selinux-policy-devel
    make
    sudo semodule -i midigatt.pp

Uninstall:

    sudo semodule -r midigatt

